<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.*, java.sql.*, javax.servlet.http.*" %>
<%!
 Connection DB_Connection() throws ClassNotFoundException, SQLException, Exception {
  String url = "jdbc:mysql://localhost:3306/FIRE";
  String id = "root";
  String pass = "gksrnr123456";
  Class.forName( "com.mysql.jdbc.Driver" ).newInstance();
  Connection conn = DriverManager.getConnection( url, id, pass );
  return conn;
 }


  String TO_DB( String str ) throws Exception
 {
   try{
    if(str != null ) return new String(str.getBytes("UTF-8"), "UTF-8");
    else return str;
   } catch(Exception e){}
   return "";

  /** if( str != null )
   return new String( str.getBytes( "UTF-8" ), "UTF-8" );
  return  ""; */
 }
 
%>   
<%
 Connection conn = DB_Connection();
 Statement stmt = null;
 ResultSet rs = null;
 

 Vector key = new Vector();
 Vector latitude = new Vector();
 Vector longitude = new Vector();
 Vector smoke = new Vector();
 Vector humi = new Vector();
 Vector temp = new Vector();
 Vector fire = new Vector();
 Vector time = new Vector();

 String productKey = request.getParameter( "key" );

 String sql = "select * from data where productKey = " + productKey;
try {
  stmt = conn.createStatement();
  rs = stmt.executeQuery( sql );
  if( rs.next() )
  {
   key.addElement( rs.getString("productKey") );
   latitude.addElement( rs.getString("latitude") );
   longitude.addElement( rs.getString("longitude") );
   smoke.addElement( rs.getString("smoke") );
   //humi.addElement( rs.getString("humi") );
   temp.addElement( rs.getString("temp") );
   fire.addElement( rs.getString("fire") );
   time.addElement( rs.getString("last") );
  }
  rs.close();
  stmt.close();
 } catch( SQLException e ) {
  out.println( e.toString() );
 }
%>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Data</title>
</head>
<body>

 <!-- 글 가져오기 -->
<table cellspacing = 0 cellpadding = 7 border = 1 width=500>
 <tr><td><b>제품 번호</b></td><td><%=key.elementAt(0)%></td></tr>
 <tr><td><b>경도</b></td><td><%=latitude.elementAt(0)%></td></tr>
 <tr><td><b>위도</b></td><td><%=longitude.elementAt(0)%></td></tr>
 <tr><td><b>연기</b></td><td><%=smoke.elementAt(0)%></td></tr>
 <tr><td><b>온도</b></td><td><%=temp.elementAt(0)%></td></tr>
 <tr><td><b>불</b></td><td><%=fire.elementAt(0)%></td></tr>
 <tr><td><b>마지막 업로드</b></td><td><%=time.elementAt(0)%></td></tr>
 
 <a class="key"><%=key.elementAt(0)%></a>
 <a class="latitude"><%=latitude.elementAt(0)%></a>
 <a class="longitude"><%=longitude.elementAt(0)%></a>
 <a class="smoke"><%=smoke.elementAt(0)%></a>
 <a class="temp"><%=temp.elementAt(0)%></a>
 <a class="fire"><%=fire.elementAt(0)%></a>
 <a class="time"><%=time.elementAt(0)%></a>
</table>


</body>
</html>